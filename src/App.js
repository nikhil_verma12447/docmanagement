import React from 'react';
import { Provider } from "react-redux";
import { HashRouter as Router, Route } from "react-router-dom";
import "./css/common.css";
import "bootstrap/dist/css/bootstrap.css";
import store from "./redux/store/store";
import Loader from "./components/loader";
import PopupMessage from "./components/PopupMessage";
import LoginSignup from "./containers/LoginSignup";
import Home from "./containers/Home";

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Loader />
        <PopupMessage/>
        <Route path="/home">
          <Home />
        </Route>
        <Route exact path="/">
          <LoginSignup />
        </Route>
      </Provider>
    </Router>
  );
}

export default App;
