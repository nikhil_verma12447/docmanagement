import React, { Component } from 'react';
import { connect } from "react-redux";
import CreateFolder from "./actionsComponents/CreateFolder";
import Rename from "./actionsComponents/Rename";
import { showLoader, hideLoader } from "../redux/action/loaderAction";
import { showPopupMessage } from "../redux/action/popupMessageAction";
import { updateFileToBeUploaded, uploadFiles, updateIsCreateFolder, deleteFilesAndFolder, updateIsRename, updateIsCopy, copyFilesAndFolder, updateIsCut, moveFilesAndFolder } from "../redux/action/actionsAction";
import { getUserDocuments } from "../redux/action/documentsAction";
import { MAX_STORAGE, getTotalSizeOfSelectedFiles } from '../service';

class Actions extends Component {
    render() {
        var screen;
        if (this.props.isCreateFolder) {
            screen = <CreateFolder />;
        }
        if (this.props.isRename) {
            screen = <Rename />
        }

        return (
            <div className="p-2 border-bottom">
                <button type="button" className="btn btn-primary" onClick={() => { this.props.updateIsCreateFolder(true) }}>Create Folder</button>
                <button type="button" className="btn btn-primary ml-3" onClick={() => { this.props.updateIsRename(true) }} disabled={this.props.selectedDocuments.length === 1 && !this.props.isCopy && !this.props.isCut ? false : true}>Rename</button>
                <button type="button" className={"btn btn-primary ml-3 " + (this.props.isCopy ? "d-none" : "")} onClick={() => { this.props.updateIsCopy(true) }} disabled={this.props.selectedDocuments.length && !this.props.isCut && this.props.storage < MAX_STORAGE ? false : true}>Copy</button>
                <button type="button" className={"btn btn-primary ml-3 " + (this.props.isCopy ? "" : "d-none")} onClick={() => { this.props.copyFilesAndFolder(this.props.currentPath, this.props.selectedDocuments) }} >Past here</button>
                <button type="button" className={"btn btn-primary ml-3 " + (this.props.isCut ? "d-none" : "")} onClick={() => { this.props.updateIsCut(true) }} disabled={this.props.selectedDocuments.length && !this.props.isCopy ? false : true}>Cut</button>
                <button type="button" className={"btn btn-primary ml-3 " + (this.props.isCut ? "" : "d-none")} onClick={() => { this.props.moveFilesAndFolder(this.props.currentPath, this.props.selectedDocuments) }} >Move here</button>
                <button type="button" className="btn btn-danger ml-3" onClick={() => { this.props.deleteFilesAndFolder(this.props.currentPath, this.props.selectedDocuments) }} disabled={this.props.selectedDocuments.length && !this.props.isCopy && !this.props.isCut ? false : true}>Delete</button>
                <form className="float-right" onSubmit={event => { event.preventDefault(); this.props.upload(this.props.currentPath, this.props.fileToBeUpload) }}>
                    <input type="file" onChange={event => { this.props.updateFile(event, MAX_STORAGE - this.props.storage) }} accept=".pdf,.html,.txt,.docx,.doc" multiple disabled={this.props.storage >= MAX_STORAGE ? true : false}/>
                    <button type="submit" className="btn btn-primary" disabled={this.props.fileToBeUpload === ""}>Upload</button>
                </form>
                {screen}
            </div>
        );
    }

    componentDidUpdate() {
        if (!this.props.selectedDocuments.length && this.props.isCopy) {
            this.props.updateIsCopy(false);
        }
        if (!this.props.selectedDocuments.length && this.props.isCut) {
            this.props.updateIsCut(false);
        }
    }
}

const mapStateToProps = function ({ actions, userDocumentsDetails }) {
    var { fileToBeUpload, createFolder, rename, isCopy, isCut } = actions;
    var { currentPath, selectedDocuments, storage } = userDocumentsDetails;
    return {
        fileToBeUpload,
        currentPath,
        isCreateFolder: createFolder.isCreateFolder,
        isRename: rename.isRename,
        selectedDocuments,
        storage,
        isCopy,
        isCut
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        updateFile: function (event, freeSpace) {
            if (event.target.files.length) {
                if (freeSpace > getTotalSizeOfSelectedFiles(event.target.files)) {
                    dispatch(updateFileToBeUploaded(event.target.files));
                } else {
                    dispatch(showPopupMessage("Error", "Not enough space."));
                }
            }
        },
        upload: function (path, file) {
            var data = new FormData();
            data.append("token", localStorage.getItem("token"))
            data.append("path", path)
            for (let i = 0; i < file.length; i++) {
                data.append("file" + i, file[i]);
            }

            dispatch(showLoader());
            uploadFiles(dispatch, data).then(response => {
                dispatch(showPopupMessage("Message", response.message));
                getUserDocuments(dispatch, { token: localStorage.getItem("token"), path }).then(() => {
                    dispatch(hideLoader());
                })
            })
        },
        updateIsCreateFolder: function (value) {
            dispatch(updateIsCreateFolder(value))
        },
        updateIsRename: function (value) {
            dispatch(updateIsRename(value))
        },
        deleteFilesAndFolder: function (path, selectedDocuments) {
            dispatch(showLoader());
            deleteFilesAndFolder(dispatch, { token: localStorage.getItem("token"), selectedDocuments: JSON.stringify(selectedDocuments), path: "" }).then(response => {
                dispatch(showPopupMessage("Message", response.message))
                getUserDocuments(dispatch, { token: localStorage.getItem("token"), path }).then(() => {
                    dispatch(hideLoader());
                })
            })
        },
        updateIsCopy: function (value) {
            dispatch(updateIsCopy(value))
        },
        copyFilesAndFolder: function (path, selectedDocuments) {
            dispatch(showLoader());
            copyFilesAndFolder(dispatch, { token: localStorage.getItem("token"), path, selectedDocuments: JSON.stringify(selectedDocuments) }).then(response => {
                dispatch(showPopupMessage("Message", response.message));
                getUserDocuments(dispatch, { token: localStorage.getItem("token"), path }).then(() => {
                    dispatch(hideLoader());
                })
            }).catch(response => {
                response = JSON.parse(response.message);

                dispatch(showPopupMessage("Error", response.message));
                dispatch(hideLoader());
            });
        },
        updateIsCut: function (value) {
            dispatch(updateIsCut(value))
        },
        moveFilesAndFolder: function (path, selectedDocuments) {
            dispatch(showLoader());
            moveFilesAndFolder(dispatch, { token: localStorage.getItem("token"), path, selectedDocuments: JSON.stringify(selectedDocuments) }).then(response => {
                dispatch(showPopupMessage("Meaage", response.message));
                getUserDocuments(dispatch, { token: localStorage.getItem("token"), path }).then(() => {
                    dispatch(hideLoader());
                })
            }).catch(response => {
                response = JSON.parse(response.message);

                dispatch(showPopupMessage("Error", response.message));
                dispatch(hideLoader());
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Actions);