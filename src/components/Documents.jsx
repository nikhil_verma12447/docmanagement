import React, { Component } from 'react';
import { connect } from "react-redux";
import { getUserDocuments, selectUnselectDocuments } from "../redux/action/documentsAction";
import { showLoader, hideLoader } from "../redux/action/loaderAction";

var timer = 0; var prevent = false;
class Documents extends Component {
    componentWillMount() {
        this.props.getUserDocuments({
            path: this.props.currentPath,
            token: localStorage.getItem("token")
        })
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.currentPath.length > 1 ?
                        <div className="mb-2 px-2">
                            <img className="back" src="back.png" alt="" width="30" onClick={() => { this.props.goBack(this.props.currentPath, !this.props.isCopy && !this.props.isCut) }} />
                        </div> : ""
                }
                <div className="documents">
                    {
                        this.props.documents.map((document, index) => {
                            return (
                                <div className={"document text-center " + (this.props.selectedDocuments.includes(this.props.currentPath + "\\" + document.name) ? "document-selected" : "")} key={index} onDoubleClick={() => {
                                    clearTimeout(timer);
                                    prevent = true;
                                    if (document.isDirctory) {
                                        this.props.getUserDocuments({
                                            path: this.props.currentPath + "\\" + document.name,
                                            token: localStorage.getItem("token")
                                        }, !this.props.isCopy && !this.props.isCut)
                                    }
                                }} onClick={() => {
                                    timer = setTimeout(() => {
                                        if (!prevent) {
                                            this.props.selectUnselectDocuments(document.name);
                                        }
                                        prevent = false
                                    }, 230)
                                }}>
                                    <div>
                                        {
                                            document.isDirctory ?
                                                <img src="folder.png" alt="html" width="50" /> : ""
                                        }
                                        {
                                            document.name.includes(".html") && !document.isDirctory ?
                                                <img src="html5.png" alt="html" width="50" /> : ""
                                        }
                                        {
                                            document.name.includes(".txt") && !document.isDirctory ?
                                                <img src="list.png" alt="text" width="50" /> : ""
                                        }
                                        {
                                            document.name.includes(".docx") && !document.isDirctory ?
                                                <img src="microsoft-word.png" alt="word doc" width="50" /> : ""
                                        }
                                        {
                                            document.name.includes(".pdf") && !document.isDirctory ?
                                                <img src="pdf.png" alt="pdf" width="50" /> : ""
                                        }
                                    </div>
                                    {document.name}
                                </div>
                            )
                        })
                    }
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = function ({ userDocumentsDetails, actions }) {
    var { currentPath, documents, selectedDocuments } = userDocumentsDetails;
    var { isCopy, isCut } = actions
    return {
        currentPath,
        documents,
        selectedDocuments,
        isCopy,
        isCut
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        getUserDocuments: function (data, isClearSelectedDocuments) {
            dispatch(showLoader());
            getUserDocuments(dispatch, data, isClearSelectedDocuments).then(() => {
                dispatch(hideLoader());
            }).catch(() => {
                dispatch(hideLoader());
            })
        },
        goBack: function (currentPath, isClearSelectedDocuments) {
            var splitedPath = currentPath.split("\\");
            splitedPath.splice(splitedPath.length - 1, 1);
            var data = {
                path: splitedPath.length ? splitedPath.join("\\") : "",
                token: localStorage.getItem("token")
            }

            dispatch(showLoader());
            getUserDocuments(dispatch, data, isClearSelectedDocuments).then(() => {
                dispatch(hideLoader());
            }).catch(() => {
                dispatch(hideLoader());
            })
        },
        selectUnselectDocuments: function (documentName) {
            dispatch(selectUnselectDocuments(documentName))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Documents);