import React, { Component } from 'react';
import { connect } from "react-redux";
import { showLoader, hideLoader } from "../redux/action/loaderAction";
import { isLogin, logout } from "../redux/action/loginSignupAction";
import { searchDocuments } from "../redux/action/documentsAction";
import { MAX_STORAGE } from "../service";

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDropdown: false
        }
    }

    render() {
        return (
            <nav>
                <span>Document Management</span>
                <div className="ml-3 mt-2 w-25 h-50 position-relative" style={{ border: "1px solid rgba(0,0,0,0.2)" }}>
                    <div className="progressbar h-100 position-absolute text-center" style={{ width: `${(this.props.storage / MAX_STORAGE) * 100}%` }}>
                    </div>
                </div>
                <span className="ml-1">{(this.props.storage / 1000000).toFixed(2)} MB/1GB</span>
                <div className="ml-5">
                    <input type="text" placeholder="Search Files..." onChange={event => { this.props.searchDocuments(event) }} />
                </div>
                <div className="position-relative float-right text-center px-5 h-100 py-2" onClick={() => { this.setState({ ...this.state, showDropdown: !this.state.showDropdown }) }}>
                    <span className="cursor-pointer">{this.props.userName}</span>
                    {
                        this.state.showDropdown ?
                            <div className="dropdown-menu-new" ref={element => { this.state.dropdownRef = element }}>
                                <ul>
                                    <li onClick={() => { this.props.logout() }}>logout</li>
                                </ul>
                            </div> : ""
                    }
                </div>
            </nav>
        );
    }
}

const mapStateToProps = function ({ loginSignup, userDocumentsDetails }) {
    var { username } = loginSignup;
    var { storage } = userDocumentsDetails
    return {
        userName: username,
        storage
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        logout: function () {
            dispatch(showLoader());
            var data = { token: localStorage.getItem("token") };
            logout(data).then(() => {
                isLogin(dispatch, data).then(() => {
                    dispatch(hideLoader());
                })
            })
        },
        searchDocuments: function(event){
            dispatch(searchDocuments(event.target.value));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);