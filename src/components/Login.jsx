import React, { Component } from 'react';
import { connect } from "react-redux";
import { showLoader, hideLoader } from "../redux/action/loaderAction";
import { updateUserName, updatePassword, loginUser } from "../redux/action/loginAction";
import { updateShowSignup, isLogin } from "../redux/action/loginSignupAction";

class Login extends Component {
    state = {}
    render() {
        return (
            <React.Fragment>
                <form className="w-50 mx-auto mt-5" onSubmit={event => { event.preventDefault(); this.props.loginUser({ userName: this.props.userName, password: this.props.password }) }}>
                    <h3>Login</h3>
                    <div className="form-group mt-3">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" aria-describedby="emailHelp" placeholder="Username" onChange={event => this.props.updateUserName(event)} autoFocus/>
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" placeholder="Password" onChange={event => this.props.updatePassword(event)} />
                    </div>
                    <button type="submit" className="btn btn-primary">Login</button>
                    <div className="text-center mt-3">
                        <span className="link" onClick={() => { this.props.updateShowSignup(true) }}>Create Account</span>
                    </div>
                </form>

            </React.Fragment>
        );
    }
}

const mapStateToProps = function ({ login }) {
    var { userName, password } = login;
    return {
        userName,
        password
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        updateUserName: (event) => {
            dispatch(updateUserName(event.target.value));
        },
        updatePassword: (event) => {
            dispatch(updatePassword(event.target.value));
        },
        updateShowSignup: (value) => {
            dispatch(updateShowSignup(value))
        },
        loginUser: (data) => {
            dispatch(showLoader())
            loginUser(dispatch, data).then(() => { 
                dispatch(hideLoader()); 
                isLogin(dispatch, {
                    token: localStorage.getItem("token")
                }) 
            }).catch(() => { 
                dispatch(hideLoader());
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);