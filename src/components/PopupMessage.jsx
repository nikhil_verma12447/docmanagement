import React, { Component } from 'react';
import { connect } from "react-redux";
import { showPopupMessage, hidePopupMessage } from "../redux/action/popupMessageAction";

class PopupMessage extends Component {
    render() {
        return (
            this.props.isShowPopup ?
                <div className="popup-container">
                    <div className="popup-message w-25 mx-auto mt-3">
                        <div className="pl-3 pt-3">
                            {this.props.header}
                            <div className="float-right pr-3 cursor-pointer" onClick={() => { this.props.hidePopupMessage() }}>&#10006;</div>
                        </div>
                        <hr />
                        <div className="pt-4 pb-4 pl-3">
                            {this.props.message}
                        </div>
                        <hr />
                        <div className="text-right pr-3 pb-3">
                            <button type="button" className="btn btn-secondary" onClick={() => { this.props.hidePopupMessage() }}>Close</button>
                        </div>
                    </div>
                </div> : ""
        );
    }
}

const mapStateToProps = function ({ popupMessage }) {
    var { isShowPopup, header, message } = popupMessage;
    return {
        isShowPopup,
        header,
        message
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        hidePopupMessage: function () {
            dispatch(hidePopupMessage());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PopupMessage);