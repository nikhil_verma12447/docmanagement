import React, { Component } from 'react';
import { connect } from "react-redux";
import { showLoader, hideLoader } from "../redux/action/loaderAction";
import { updateUserName, updatePassword, updateConfirmPassword, signupUser } from "../redux/action/signupAction";
import { updateShowSignup, isLogin } from "../redux/action/loginSignupAction";

class Signup extends Component {
    render() {
        return (
            <React.Fragment>
                <form className="w-50 mx-auto mt-5" onSubmit={event => { event.preventDefault(); this.props.signupUser({ userName: this.props.userName, password: this.props.password, confirmPassword: this.props.confirmPassword }) }}>
                    <h3>Create Account</h3>
                    <div className="form-group mt-3">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" aria-describedby="emailHelp" placeholder="Username" onChange={event => this.props.updateUserName(event)} autoFocus/>
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" placeholder="Password" onChange={event => this.props.updatePassword(event)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="cnfPassword">Confirm Password</label>
                        <input type="password" className="form-control" id="cnfPassword" placeholder="Confirm Password" onChange={event => this.props.updateConfirmPassword(event)} />
                    </div>
                    <button type="submit" className="btn btn-primary">Signup</button>
                    <div className="text-center">
                        <span className="link" onClick={() => { this.props.updateShowSignup(false) }}>Login</span>
                    </div>
                </form>
            </React.Fragment>
        );
    }
}

const mapStateToProps = function ({ signup }) {
    var { userName, password, confirmPassword } = signup;
    return {
        userName,
        password,
        confirmPassword
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        updateUserName: (event) => {
            dispatch(updateUserName(event.target.value));
        },
        updatePassword: (event) => {
            dispatch(updatePassword(event.target.value));
        },
        updateConfirmPassword: (event) => {
            dispatch(updateConfirmPassword(event.target.value))
        },
        updateShowSignup: (value) => {
            dispatch(updateShowSignup(value))
        },
        signupUser: (data) => {
            console.log(data);
            dispatch(showLoader())
            signupUser(dispatch, data).then(() => { 
                dispatch(hideLoader()); 
                isLogin(dispatch, {
                    token: localStorage.getItem("token")
                });
            }).catch(() => { 
                dispatch(hideLoader());
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);