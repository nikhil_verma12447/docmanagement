import React, { Component } from 'react';
import { connect } from "react-redux";
import { showLoader, hideLoader } from "../../redux/action/loaderAction";
import { showPopupMessage } from "../../redux/action/popupMessageAction";
import { getUserDocuments } from "../../redux/action/documentsAction";
import { updateIsCreateFolder, updateFolderName, createFolder } from "../../redux/action/actionsAction";
import { isFileOrFolderExist } from '../../service';

class CreateFolder extends Component {
    render() {
        return (
            <div className="popup-container">
                <form action="" onSubmit={event => { event.preventDefault(); this.props.createFolder(this.props.currentPath, this.props.folderName, this.props.documents) }}>
                    <div className="popup-message w-25 mx-auto mt-3">
                        <div className="pl-3 pt-3">
                            <div className="text-right pr-3 cursor-pointer" onClick={() => { this.props.updateIsCreateFolder(false) }}>&#10006;</div>
                        </div>
                        <hr />
                        <div className="py-4 px-3">
                            <input className="form-control" type="text" placeholder="Enter Folder Name" onChange={event => { this.props.updateFolderName(event) }} autoFocus />
                        </div>
                        <hr />
                        <div className="text-center pr-3 pb-3">
                            <button type="submit" className="btn btn-success" disabled={this.props.folderName ? false : true}>Create</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = function ({ actions, userDocumentsDetails }) {
    var { createFolder } = actions;
    var { currentPath, documents } = userDocumentsDetails;
    return {
        currentPath,
        documents,
        folderName: createFolder.folderName
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        updateIsCreateFolder: function (value) {
            dispatch(updateIsCreateFolder(value))
        },
        updateFolderName: function (event) {
            dispatch(updateFolderName(event.target.value));
        },
        createFolder: function (path, folderName, documents) {
            if (isFileOrFolderExist(folderName, documents)) {
                dispatch(updateIsCreateFolder(false))
                dispatch(showPopupMessage("Error", "Folder already exist."))
            } else {
                dispatch(showLoader());
                createFolder(dispatch, { token: localStorage.getItem("token"), path, folderName }).then(() => {
                    getUserDocuments(dispatch, { token: localStorage.getItem("token"), path }).then(() => {
                        dispatch(updateIsCreateFolder(false));
                        dispatch(hideLoader());
                    })
                })
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateFolder);