import React, { Component } from 'react';
import { connect } from "react-redux";
import { showLoader, hideLoader } from "../../redux/action/loaderAction";
import { showPopupMessage } from "../../redux/action/popupMessageAction";
import { getUserDocuments } from "../../redux/action/documentsAction";
import { updateIsRename, updateNewName, renameFilesAndFolder } from "../../redux/action/actionsAction";
import { isFileOrFolderExist } from '../../service';

class Rename extends Component {
    render() {
        return (
            <div className="popup-container">
                <form action="" onSubmit={event => { event.preventDefault(); this.props.renameFilesAndFolder(this.props.currentPath, this.props.documents, this.props.newName, this.props.selectedDocuments) }}>
                    <div className="popup-message w-25 mx-auto mt-3">
                        <div className="pl-3 pt-3">
                            <div className="text-right pr-3 cursor-pointer" onClick={() => { this.props.updateIsRename(false) }}>&#10006;</div>
                        </div>
                        <hr />
                        <div className="py-4 px-3">
                            <input className="form-control" type="text" placeholder="Enter New Name" onChange={event => { this.props.updateNewName(event) }} autoFocus />
                        </div>
                        <hr />
                        <div className="text-center pr-3 pb-3">
                            <button type="submit" className="btn btn-success" disabled={this.props.newName ? false : true}>Rename</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = function ({ actions, userDocumentsDetails }) {
    var { createFolder, rename } = actions;
    var { currentPath, documents, selectedDocuments } = userDocumentsDetails;
    return {
        currentPath,
        documents,
        selectedDocuments,
        folderName: createFolder.folderName,
        newName: rename.newName
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        updateIsRename: function (value) {
            dispatch(updateIsRename(value))
        },
        updateNewName: function (event) {
            dispatch(updateNewName(event.target.value));
        },
        renameFilesAndFolder: function (path, documents, newName, selectedDocuments) {
            var temp = selectedDocuments[0].split("\\");
            var temp1 = temp[temp.length - 1].split(".");

            if (isFileOrFolderExist(newName, documents)) {
                dispatch(updateIsRename(false))
                dispatch(showPopupMessage("Error", "Already exist."))
            } else {
                dispatch(showLoader());
                renameFilesAndFolder(dispatch, { token: localStorage.getItem("token"), path, oldName: temp[temp.length - 1], newName: temp[temp.length - 1].includes(".") ? newName + "." + temp1[temp1.length - 1] : newName }).then(() => {
                    getUserDocuments(dispatch, { token: localStorage.getItem("token"), path }).then(() => {
                        dispatch(updateIsRename(false));
                        dispatch(hideLoader());
                    })
                })
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Rename);