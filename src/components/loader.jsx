import React, { Component } from 'react';
import { connect } from "react-redux";
import { showLoader, hideLoader } from "../redux/action/loaderAction";

class Loader extends Component {
    render() {
        return (
            this.props.isShowLoader ?
                <div className="popup-container">
                    <div className="loader mx-auto"></div>
                </div> : ""
        );
    }
}

const mapStateToProps = function ({ loader }) {
    var { showLoader } = loader;
    return {
        isShowLoader: showLoader
    }
}

const mapDispatchToProps = function (dispatch) {
    return {
        showLoader: () => {
            dispatch(showLoader())
        },
        hideLoader: () => {
            dispatch(hideLoader())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loader);