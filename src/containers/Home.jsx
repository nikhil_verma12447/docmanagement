import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Header from "../components/Header";
import Actions from "../components/Actions";
import Documents from "../components/Documents";

class Home extends Component {
    render() {
        return (
            this.props.isLogin ?
                <React.Fragment>
                    <Header />
                    <Actions />
                    <Documents />
                </React.Fragment> :
                <Redirect to="/" />
        );
    }
}

const mapStateToProps = function ({ loginSignup }) {
    var { isLogin } = loginSignup;
    return {
        isLogin
    }
}

const mapDispatchToProps = function (dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);