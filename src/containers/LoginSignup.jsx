import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Login from "../components/Login";
import Signup from "../components/Signup";

class LoginSignup extends Component {
    render() {
        return (
            this.props.isLogin ?
                <Redirect to="/home" /> :
                this.props.showSignup ?
                    <Signup /> : <Login />
        );
    }
}

const mapStateToProps = function ({ loginSignup }) {
    var { showSignup, isLogin } = loginSignup;
    return {
        showSignup,
        isLogin
    }
}

const mapDispatchToProps = function (dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginSignup);