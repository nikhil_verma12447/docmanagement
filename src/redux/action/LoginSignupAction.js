import { callApi } from "../../service"

export function updateShowSignup(value) {
    return {
        type: "UPDATE_SHOW_SIGNUP",
        payload: value
    }
}

export function logout(data) {
    return callApi("post", "/logout", data)
}

export function isLogin(dispatch, data) {
    return callApi("post", "/isLogin", data).then(response => {
        dispatch({
            type: "IS_LOGIN_FULFILLED",
            payload: response.data
        })
    }).catch(response => {
        dispatch({
            type: "IS_LOGIN_REJECTED",
            payload: response.data
        })
    })
}