import { callApi } from "../../service";

export function updateFileToBeUploaded(file) {
    return {
        type: "UPDATE_FILE_TO_BE_UPLOADED",
        payload: file
    }
}

export function updateIsCreateFolder(value) {
    return {
        type: "UPDATE_IS_CREATE_FOLDER",
        payload: value
    }
}

export function updateFolderName(folderName) {
    return {
        type: "UPDATE_FOLDER_NAME",
        payload: folderName
    }
}

export function updateIsRename(value) {
    return {
        type: "UPDATE_IS_RENAME",
        payload: value
    }
}

export function updateNewName(newName) {
    return {
        type: "UPDATE_NEW_NAME",
        payload: newName
    }
}

export function updateIsCopy(value) {
    return {
        type: "UPDATE_IS_COPY",
        payload: value
    }
}

export function updateIsCut(value) {
    return {
        type: "UPDATE_IS_CUT",
        payload: value
    }
}

export function uploadFiles(dispatch, data) {
    return callApi("post", "/upload", data).then(response => {
        dispatch({
            type: "UPLOAD_FILES_FOLDER_FULFILLMENT",
            payload: response.data
        })
        return response.data;
    }).catch(response => {
        dispatch({
            type: "UPLOAD_FILES_FOLDER_REJECTED",
            payload: response.data
        })
    });
}

export function createFolder(dispatch, data) {
    return callApi("post", "/action?action=create", data).then(response => {
        dispatch({
            type: "CREATE_FOLDER_FULFILLMENT",
            payload: response.data
        })
        return response.data;
    }).catch(response => {
        dispatch({
            type: "CREATE_FOLDER_REJECTED",
            payload: response.data
        })
    });
}

export function deleteFilesAndFolder(dispatch, data) {
    return callApi("post", "/action?action=delete", data).then(response => {
        dispatch({
            type: "DELETE_FILES_FOLDER_FULFILLMENT",
            payload: response.data
        })
        return response.data;
    }).catch(response => {
        dispatch({
            type: "DELETE_FILES_FOLDER_REJECTED",
            payload: response.data
        })
    });
}

export function copyFilesAndFolder(dispatch, data) {
    return callApi("post", "/action?action=copy", data).then(response => {
        dispatch({
            type: "COPY_FILES_FOLDER_FULFILLMENT",
            payload: response.data
        })
        return response.data;
    }).catch(({ response }) => {
        dispatch({
            type: "COPY_FILES_FOLDER_REJECTED",
            payload: response.data
        })
        throw new Error(JSON.stringify(response.data));
    });
}

export function moveFilesAndFolder(dispatch, data) {
    return callApi("post", "/action?action=move", data).then(response => {
        dispatch({
            type: "MOVE_FILES_FOLDER_FULFILLMENT",
            payload: response.data
        })
        return response.data;
    }).catch(({response}) => {
        dispatch({
            type: "MOVE_FILES_FOLDER_REJECTED",
            payload: response.data
        })
        throw new Error(JSON.stringify(response.data));
    });
}

export function renameFilesAndFolder(dispatch, data) {
    return callApi("post", "/action?action=rename", data).then(response => {
        dispatch({
            type: "RENAME_FILES_FOLDER_FULFILLMENT",
            payload: response.data
        })
        return response.data;
    }).catch(response => {
        dispatch({
            type: "RENAME_FILES_FOLDER_REJECTED",
            payload: response.data
        })
    });
}