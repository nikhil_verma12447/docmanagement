import { callApi } from "../../service";

export function getUserDocuments(dispatch, data, isClearSelectedDocuments = true) {
    return callApi("post", "/documents", data).then(response => {
        dispatch({
            type: "GET_USER_DOCUMENTS_FULFILLED",
            payload: {
                data: response.data,
                isClearSelectedDocuments
            }
        })
    }).catch(response => {
        dispatch({
            type: "GET_USER_DOCUMENTS_REJECTED",
            payload: response.data
        })
    })
}

export function selectUnselectDocuments(documentName){
    return {
        type: "SELECT_UNSELECT_DOCUMENTS",
        payload: documentName
    }
}

export function searchDocuments(searchString){
    return {
        type: "SEARCH_DOCUMENTS",
        payload: searchString
    }
}