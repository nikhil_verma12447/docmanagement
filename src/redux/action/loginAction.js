import { callApi } from "../../service";

export function updateUserName(userName) {
    return {
        type: "LOGIN_UPDATE_USERNAME",
        payload: userName
    }
}

export function updatePassword(password) {
    return {
        type: "LOGIN_UPDATE_PASSWORD",
        payload: password
    }
}

export function loginUser(dispatch, data) {
    return callApi("post", "/login", data).then(response => {
        dispatch({
            type: "LOGIN_USER_FULFILLED",
            payload: response.data
        });
    }).catch(response => {
        dispatch({
            type: "LOGIN_USER_REJECTED",
            payload: response.data
        });
    })
}