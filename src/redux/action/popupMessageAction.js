export function showPopupMessage(header, message){
    return {
        type: "SHOW_POPUP_MESSAGE",
        payload: {
            header,
            message
        }
    }
}

export function hidePopupMessage(){
    return {
        type: "HIDE_POPUP_MESSAGE",
        payload: ""
    }
}