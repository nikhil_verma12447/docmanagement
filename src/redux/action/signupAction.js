import { callApi } from "../../service"

export function updateUserName(userName) {
    return {
        type: "SIGNUP_UPDATE_USERNAME",
        payload: userName
    }
}

export function updatePassword(password) {
    return {
        type: "SIGNUP_UPDATE_PASSWORD",
        payload: password
    }
}

export function updateConfirmPassword(confirmPassword) {
    return {
        type: "SIGNUP_UPDATE_CONFIRM_PASSWORD",
        payload: confirmPassword
    }
}

export function signupUser(dispatch, data) {
    return callApi("post", "/signup", data).then(response => {
        dispatch({
            type: "SIGNUP_USER_FULFILLED",
            payload: response.data
        });
    }).catch(response => {
        dispatch({
            type: "SIGNUP_USER_REJECTED",
            payload: response.data
        });
    });
}