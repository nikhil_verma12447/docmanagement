const loginSignup = function (state = {
    username: "",
    showSignup: false,
    isLogin: false
}, action) {
    switch (action.type) {
        case "UPDATE_SHOW_SIGNUP":
            state = {
                ...state,
                showSignup: action.payload
            }
            break;
        case "IS_LOGIN_FULFILLED":
            state = {
                ...state,
                isLogin: action.payload.result.isLogin,
                username: action.payload.result.username
            }
            break;
        case "IS_LOGIN_REJECTED":
            break;
    }
    return state;
}

export default loginSignup;