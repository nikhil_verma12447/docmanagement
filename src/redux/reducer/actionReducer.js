const actions = function (state = {
    fileToBeUpload: "",
    createFolder: {
        isCreateFolder: false,
        folderName: "",
    },
    rename: {
        isRename: false,
        newName: ""
    },
    isCopy: false,
    isCut: false,
    error: false,
    errorCode: ""
}, action) {
    switch (action.type) {
        case "UPDATE_FILE_TO_BE_UPLOADED":
            state = {
                ...state,
                fileToBeUpload: action.payload
            }
            break;
        case "UPLOAD_FILES_FOLDER_FULFILLMENT":
            state = {
                ...state,
                fileToBeUpload: ""
            }
            break;
        case "UPLOAD_FILES_FOLDER_REJECTED":
            break;
        case "UPDATE_IS_CREATE_FOLDER":
            state = {
                ...state,
                createFolder: {
                    ...state.createFolder,
                    isCreateFolder: action.payload
                }
            }
            break;
        case "UPDATE_FOLDER_NAME":
            state = {
                ...state,
                createFolder: {
                    ...state.createFolder,
                    folderName: action.payload.trim()
                }
            }
            break;
        case "UPDATE_IS_RENAME":
            state = {
                ...state,
                rename: {
                    ...state.rename,
                    isRename: action.payload
                }
            }
            break;
        case "UPDATE_NEW_NAME":
            state = {
                ...state,
                rename: {
                    ...state.rename,
                    newName: action.payload
                }
            }
            break;
        case "UPDATE_IS_COPY":
            state = {
                ...state,
                isCopy: action.payload,
                isCut: false
            }
            break;
        case "UPDATE_IS_CUT":
            state = {
                ...state,
                isCut: action.payload,
                isCopy: false
            }
            break;
    }
    return state;
}

export default actions;