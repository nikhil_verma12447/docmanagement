const userDocumentsDetails = function (state = {
    currentPath: "",
    documents: [],
    backupDocuments: [],
    selectedDocuments: [],
    storage: 0,
    searchString: "",
    error: false,
    errorCode: ""
}, action) {
    switch (action.type) {
        case "GET_USER_DOCUMENTS_FULFILLED":
            state = {
                ...state,
                selectedDocuments: action.payload.isClearSelectedDocuments ? [] : [...state.selectedDocuments],
                currentPath: action.payload.data.result.path,
                documents: action.payload.data.result.documents,
                backupDocuments: action.payload.data.result.documents,
                storage: action.payload.data.result.storage
            }
            break;
        case "GET_USER_DOCUMENTS_REJECTED":
            state = {
                ...state,
                error: action.payload.error,
                errorCode: action.payload.code
            }
            break;
        case "SELECT_UNSELECT_DOCUMENTS":
            var { selectedDocuments, currentPath } = state
            selectedDocuments.includes(currentPath + "\\" + action.payload) ? selectedDocuments.splice(selectedDocuments.indexOf(currentPath + "\\" + action.payload), 1) : selectedDocuments.push(currentPath + "\\" + action.payload);
            state = {
                ...state,
                selectedDocuments: [...selectedDocuments]
            }
            break;
        case "SEARCH_DOCUMENTS":
            var filterDocuments;
            if(action.payload){
                filterDocuments = state.backupDocuments.filter(document => {
                    return document.name.includes(action.payload);
                })
            }else{
                filterDocuments = state.backupDocuments;
            }
            
            state = {
                ...state,
                documents: [...filterDocuments]
            }
            break;
    }
    return state;
}

export default userDocumentsDetails;