const loader = function (state = {
    showLoader: false
}, action) {
    switch (action.type) {
        case "SHOW_LOADER":
            state = {
                ...state,
                showLoader: action.payload
            }
            break;
        case "HIDE_LOADER":
            state = {
                ...state,
                showLoader: action.payload
            }
            break;
    }
    return state;
}

export default loader;