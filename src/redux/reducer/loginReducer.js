const login = function (state = {
    userName: "",
    password: "",
    error: false,
    errorCode: ""
}, action) {
    switch (action.type) {
        case "LOGIN_UPDATE_USERNAME":
            state = {
                ...state,
                userName: action.payload
            }
            break;
        case "LOGIN_UPDATE_PASSWORD":
            state = {
                ...state,
                password: action.payload
            }
            break;
        case "LOGIN_USER_FULFILLED":
            localStorage.setItem("token", action.payload.result.token)
            break;
        case "LOGIN_USER_REJECTED":
            state = {
                error: action.payload.error,
                errorCode: action.payload.code
            }
            break;
    }
    return state;
}

export default login;