const popupMessage = function (state = {
    isShowPopup: false,
    header: "",
    message: "",
}, action) {
    switch (action.type) {
        case "SHOW_POPUP_MESSAGE":
            state = {
                ...state,
                isShowPopup: true,
                header: action.payload.header,
                message: action.payload.message
            }
            break;
        case "HIDE_POPUP_MESSAGE":
            state = {
                ...state,
                isShowPopup: false,
                header: "",
                message: ""
            }
            break;
    }
    return state;
}

export default popupMessage;