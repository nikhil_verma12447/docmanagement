const signup = function (state = {
    userName: "",
    password: "",
    confirmPassword: "",
    error: false,
    errorCode: ""
}, action) {
    switch (action.type) {
        case "SIGNUP_UPDATE_USERNAME":
            state = {
                ...state,
                userName: action.payload
            }
            break;
        case "SIGNUP_UPDATE_PASSWORD":
            state = {
                ...state,
                password: action.payload
            }
            break;
        case "SIGNUP_UPDATE_CONFIRM_PASSWORD":
            state = {
                ...state,
                confirmPassword: action.payload
            }
            break;
        case "SIGNUP_USER_FULFILLED":
            localStorage.setItem("token", action.payload.result.token)
            break;
        case "SIGNUP_USER_REJECTED":
            state = {
                ...state,
                error: action.payload.error,
                errorCode: action.payload.code
            }
            break;
    }

    return state;
}

export default signup;