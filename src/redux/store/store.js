import { createStore, combineReducers } from "redux";
import loader from "../reducer/loaderReducer";
import login from "../reducer/loginReducer";
import signup from "../reducer/signupReducer";
import loginSignup from "../reducer/loginSignupReducer";
import userDocumentsDetails from "../reducer/documentsReducer";
import actions from "../reducer/actionReducer";
import popupMessage from "../reducer/popupMessgaeReducer";

var store = createStore(combineReducers({
    loader,
    login,
    signup,
    loginSignup,
    userDocumentsDetails,
    actions,
    popupMessage
}));

export default store;