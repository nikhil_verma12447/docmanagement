import axios from "axios";

export const MAX_STORAGE = 1000000000;
const BASE_URL = "http://localhost:8080";

export function callApi(method, url, data, headers = {}) {
    url = BASE_URL + url;
    switch (method.toUpperCase()) {
        case "GET":
            return axios.get(url);
        case "POST":
            return axios.post(url, data, { headers });
    }
}

export function isFileOrFolderExist(name, documents){
    var value = false;
    documents.forEach(document => {
        if(document.name === name){
            value = true;
        }
    })
    return value;
}

export function getTotalSizeOfSelectedFiles(files){
    var size = 0;
    for(let i=0; i<files.length; i++){
        size += files[i].size
    }
    
    return size;
}